import {Component, OnInit} from '@angular/core';
import {LoaderService} from './services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  public showLoader: boolean;
  private lock: boolean;
  private queue: Array<boolean> = [];

  constructor(private loaderService: LoaderService) {
  }

  ngOnInit() {
    this.loaderService.activate().subscribe(e => this.loaderEvent(e));
  }

  private wait(ms) {
    const start = new Date().getTime();
    let end = start;
    while (end < start + ms) {
      end = new Date().getTime();
    }
  }

  private synchronized() {

  }

  public loaderEvent(e: boolean) {
    while (this.lock) {
      this.wait(2000);
    }
    this.lock = true;
    if (e) {
      this.queue.push(e);
      this.showLoader = true;
    } else if (this.queue.length > 0) {
      this.queue.pop();
    }
    if (this.queue.length === 0) {
      this.showLoader = false;
    }
    this.lock = false;
  }
}
