import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoaderService} from '../loader/services/loader.service';
import {MessageService} from '../message/services/message.service';


@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private loader: LoaderService,
              private messageService: MessageService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    setTimeout(() => {
      this.messageService.showMessage(null);
      this.loader.fireLoaderEvent(true);
    }, 0);
    return next.handle(request);
  }
}
