import {Injectable} from '@angular/core';
import {SystemMessage} from '../model/message.model';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private msgSub: Subject<SystemMessage> = new Subject<SystemMessage>();

  constructor() {
  }

  public addMessageListener(): Observable<SystemMessage> {
    return this.msgSub.asObservable();
  }

  public showMessage(msg: SystemMessage) {
    this.msgSub.next(msg);
  }



}
