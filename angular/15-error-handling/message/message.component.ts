import {Component, OnInit} from '@angular/core';
import {MessageService} from './services/message.service';
import {SystemMessage} from './model/message.model';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  message: SystemMessage;
  timeout: any;

  constructor(private messageService: MessageService) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.messageService.addMessageListener().subscribe(m => {
      this.message = m;
      this.timeout = setTimeout(() => {
        this.message = null;
      }, 10000);
    });
  }

  ngOnInit() {
  }

  closeMessageBox() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.message = null;
  }
}
