import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private loadObs: Subject<boolean> = new Subject<boolean>();

  constructor() {

  }

  public activate(): Observable<boolean> {
    return this.loadObs.asObservable();
  }

  public fireLoaderEvent(e: boolean) {
    this.loadObs.next(e);
  }
}
