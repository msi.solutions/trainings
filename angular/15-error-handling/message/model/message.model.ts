export enum MessageType {
  ERROR,
  INFO,
  WARNING
}

export class SystemMessage {
  title: string;
  message: string;
  messageI18nKey: string;
  defaultMessage: string;
  type: MessageType;
}

export class MessageBuilder {
  private msg: SystemMessage = new SystemMessage();

  public withTitle(title: string) {
    this.msg.title = title;
    return this;
  }

  public withMessage(msg: string) {
    this.msg.message = msg;
    return this;
  }


  public withMessageType(type: MessageType) {
    this.msg.type = type;
    return this;
  }

  public withMessageKey(key: string) {
    this.msg.messageI18nKey = key;
    return this;
  }

  public withDefaultMessage(dm: string) {
    this.msg.defaultMessage = dm;
    return this;
  }


  public build(): SystemMessage {
    return this.msg;
  }
}
