import { Injectable } from '@angular/core';

import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoaderService } from '../loader/services/loader.service';
import { MessageService } from '../message/services/message.service';
import { MessageBuilder, MessageType } from '../message/model/message.model';
import { tap } from 'rxjs/operators';


@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService,
              private messageService: MessageService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        setTimeout(() => this.loaderService.fireLoaderEvent(false), 50);
        if (event['status']) {
          const code = event['status'];
          if (code !== 200) {
            setTimeout(() => {
              this.messageService.showMessage(new MessageBuilder()
                .withMessage(`Server error :${code}`)
                .withMessageType(MessageType.ERROR)
                .withMessageKey('message key')
                .withDefaultMessage('defaultMessage')
                .build());
              if (event['body']['consoleMessage']) {
                console.error(event['body']['consoleMessage']);
              }
            }, 0);
          }

        }
      }
    }, (err: any) => {
      setTimeout(() => this.loaderService.fireLoaderEvent(false), 50);
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          this.messageService.showMessage(new MessageBuilder()
            .withMessage('cloud.default.http.401')
            .withTitle('cloud.security.title.error')
            .withMessageType(MessageType.ERROR)
            .build());
        }
        if (err.status === 404) {
          this.messageService.showMessage(new MessageBuilder()
            .withMessage('cloud.default.http.404')
            .withTitle('cloud.default.data.missing')
            .withMessageType(MessageType.ERROR)
            .build());
        }
        if (err.status === 500) {
          this.messageService.showMessage(new MessageBuilder()
            .withMessage('cloud.default.http.500')
            .withTitle('cloud.default.system.title')
            .withMessageType(MessageType.WARNING)
            .build());
        }
        if (err.status === 400) {
          this.messageService.showMessage(new MessageBuilder()
            .withMessage('cloud.default.http.400')
            .withTitle('cloud.default.validation.title')
            .withMessageType(MessageType.ERROR)
            .build());
        }
      }
    }));
  }

}
